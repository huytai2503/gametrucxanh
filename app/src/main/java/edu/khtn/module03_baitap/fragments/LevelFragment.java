package edu.khtn.module03_baitap.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import edu.khtn.module03_baitap.R;

public class LevelFragment extends Fragment {
    RadioGroup radioGroup;
    Button btnChosen;
    EditText edtTime;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_level, null, false);
        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        edtTime = (EditText) view.findViewById(R.id.edit_time);
        btnChosen = (Button) view.findViewById(R.id.button_chosen);

        btnChosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getString = edtTime.getText().toString();
                int getTime = 60;
                if (!getString.isEmpty()){
                    getTime = Integer.parseInt(getString);
                }
                HomeFragment.chosenTime = getTime;
                getFragmentManager().popBackStack();
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_4:
                        HomeFragment.chosenLevel = 4;
                        break;
                    case R.id.radio_6:
                        HomeFragment.chosenLevel = 6;
                        break;
                    case R.id.radio_12:
                        HomeFragment.chosenLevel = 12;
                        break;
                    case R.id.radio_16:
                        HomeFragment.chosenLevel = 16;
                        break;
                    case R.id.radio_20:
                        HomeFragment.chosenLevel = 20;
                        break;
                    case R.id.radio_30:
                        HomeFragment.chosenLevel = 30;
                        break;
                    case R.id.radio_36:
                        HomeFragment.chosenLevel = 36;
                        break;
                }
            }
        });

        return view;
    }
}
