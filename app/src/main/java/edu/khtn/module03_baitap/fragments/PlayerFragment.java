package edu.khtn.module03_baitap.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.khtn.module03_baitap.R;

public class PlayerFragment extends Fragment implements View.OnClickListener {
    LinearLayout parentLayout;
    RelativeLayout subLayout;
    Button btnBack, btnStart;
    TextView textTimer;
    List<Integer> resList = new ArrayList();
    List imgList = new ArrayList();
    Random random = new Random();
    boolean isPause = true;
    String score = "";
    int changeTime = 2000, srcImg, congratImg;
    int chosenId1 = 0, chosenId2 = 0;
    int layoutWidth = 0, layoutHeight = 0;
    int countHeight = 0, countWidth = 0;
    int minSize = 0, mLoop, sLoop;
    int imgTotal = 0, imgGet = 0;
    int minutesPassed = 0, minutesTotal = 0;
    int secondsPassed = 0, secondsTotal = 61;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, null, false);
        linkViewsAndSetListener(view);
        congratImg = R.drawable.congratulation;
        srcImg = R.drawable.ic_launcher;
        imgTotal = getArguments().getInt("chosenLevel");
        minutesTotal = getArguments().getInt("chosenTime");
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                layoutHeight = parentLayout.getHeight();
                layoutWidth = parentLayout.getWidth();
                chosenMinSize();
                createResList();
                createImgList();
                addButtonToLayout();
                setClickableView(false);
            }
        }, 100);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        stopTimer();
    }

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            getFragmentManager().popBackStack();
        } else if (v == btnStart) {
            if (isPause) {
                isPause = false;
                btnStart.setText("Pause");
                createAsynctask();
                setClickableView(true);
            } else {
                stopTimer();
                setClickableView(false);
            }
        }
    }

    public void stopTimer() {
        mLoop = minutesTotal;
        sLoop = secondsTotal;
        isPause = true;
        btnStart.setText("Start");
    }

    public void createAsynctask() {
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                for (mLoop = minutesPassed; mLoop < minutesTotal; mLoop++) {
                    System.out.println("mLoop: " + mLoop);
                    for (sLoop = secondsPassed; sLoop < secondsTotal; ) {
                        sLoop++;
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (sLoop < secondsTotal) {
                            publishProgress(new Integer[]{sLoop});
                            System.out.println("sLoop: " + sLoop);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Object[] values) {
                secondsPassed = (int) values[0];
                createTimer();
            }
        };
        asyncTask.execute();
    }

    public void createTimer() {
        String sMinutes = "", sSeconds = "";
        if (secondsPassed == secondsTotal - 1) {
            secondsPassed = 0;
            minutesPassed++;
        }
        if (secondsPassed < 10) {
            sSeconds = "0" + secondsPassed;
        } else {
            sSeconds = secondsPassed + "";
        }
        if (minutesPassed < 10) {
            sMinutes = "0" + minutesPassed;
        } else {
            sMinutes = minutesPassed + "";
        }
        textTimer.setText(sMinutes + ":" + sSeconds);
    }

    public void chosenMinSize() {
        int heightMin, heightMax;
        int countMin = 0, countMax = 0;
        int temp = (int) Math.sqrt(imgTotal);
        if (imgTotal % temp == 0) {
            countMin = temp;
            countMax = imgTotal / countMin;
        }
        if (layoutWidth > layoutHeight) {
            heightMin = layoutHeight;
            heightMax = layoutWidth;
            countWidth = countMax;
            countHeight = countMin;
        } else {
            heightMin = layoutWidth;
            heightMax = layoutHeight;
            countWidth = countMin;
            countHeight = countMax;
        }
        int sizeA = heightMin / countMin;
        int sizeB = heightMax / countMax;
        if (sizeA < sizeB) {
            minSize = sizeA;
        } else {
            minSize = sizeB;
        }
    }

    public void addButtonToLayout() {
        int freeHeightSpace = layoutHeight - (countHeight * minSize);
        int freeWidghSpace = layoutWidth - (countWidth * minSize);
        int i = 0;
        for (int row = 0; row < countHeight; row++) {
            LinearLayout layout = createLayout();
            parentLayout.setPadding(freeWidghSpace / 2, freeHeightSpace / 2, 0, 0);
            parentLayout.addView(layout);
            for (int col = 0; col < countWidth; col++) {
                Button button = createButton((Integer) imgList.get(i));
                layout.addView(button);
                i++;
            }
        }
    }

    public LinearLayout createLayout() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, minSize);
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setLayoutParams(params);
        return layout;
    }

    public Button createButton(final int resId) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (minSize, LinearLayout.LayoutParams.MATCH_PARENT);
        final Button button = new Button(getActivity());
        button.setBackgroundResource(R.drawable.ic_launcher);
        button.setLayoutParams(params);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setBackgroundResource(resId);
                if (chosenId1 == 0) {
                    chosenId1 = resId;
                    changeImage(button, changeTime);
                } else if (chosenId2 == 0) {
                    chosenId2 = resId;
                    changeImage(button, changeTime - 500);
                    showProgressBar(changeTime - 500);
                }
                if (chosenId1 == chosenId2) {
                    chosenId1 = 0;
                    chosenId2 = 0;
                    imgGet += 2;
                    if (imgGet == imgTotal) {
                        stopTimer();
                        v.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                parentLayout.removeAllViews();
                                parentLayout.setBackgroundResource(congratImg);
                                score = imgTotal + "#" + textTimer.getText() + "";
                                showToast(score);
                            }
                        }, 1000);
                    }
                }
            }
        });
        return button;
    }

    public void changeImage(final Button button, int milis) {
        View v = new View(getActivity());
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (chosenId1 != chosenId2) {
                    button.setBackgroundResource(srcImg);
                    if (chosenId2 == 0) {
                        chosenId1 = 0;
                    } else {
                        chosenId2 = 0;
                    }
                }
            }
        }, milis);
    }

    public void showProgressBar(int milis) {
        View v = new View(getActivity());
        final ProgressBar progressBar = new ProgressBar(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.
                LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        progressBar.setLayoutParams(params);
        subLayout.addView(progressBar);
        setClickableView(false);
        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                subLayout.removeView(progressBar);
                setClickableView(true);
            }
        }, milis);
    }

    public void setClickableView(boolean clickable) {
        for (int i = 0; i < parentLayout.getChildCount(); i++) {
            View view = parentLayout.getChildAt(i);
            LinearLayout subLayout = (LinearLayout) view;
            for (int j = 0; j < subLayout.getChildCount(); j++) {
                View child = subLayout.getChildAt(j);
                child.setClickable(clickable);
            }
        }
    }

    public void createImgList() {
        List randomList = new ArrayList();
        int imgCount = imgTotal / 2;
        List list1 = new ArrayList();
        List list2 = new ArrayList();
        int index1, index2, imgIdx;
        for (int i = 0; i < imgCount; i++) {
            do {
                imgIdx = random.nextInt(resList.size());
            } while (randomList.contains(resList.get(imgIdx)));
            randomList.add(resList.get(imgIdx));
        }
        for (int i = 0; i < imgCount; i++) {
            do {
                index1 = random.nextInt(imgCount);
            } while (list1.contains(index1));
            list1.add(index1);
            do {
                index2 = random.nextInt(imgCount);
            } while (list2.contains(index2));
            list2.add(index2);
            imgList.add(randomList.get(index1));
            imgList.add(randomList.get(index2));
        }
    }

    public void createResList() {
        resList.add(R.drawable.p1);
        resList.add(R.drawable.p2);
        resList.add(R.drawable.p3);
        resList.add(R.drawable.p4);
        resList.add(R.drawable.p5);
        resList.add(R.drawable.p6);
        resList.add(R.drawable.p7);
        resList.add(R.drawable.p8);
        resList.add(R.drawable.p9);
        resList.add(R.drawable.p10);
        resList.add(R.drawable.p11);
        resList.add(R.drawable.p12);
        resList.add(R.drawable.p13);
        resList.add(R.drawable.p14);
        resList.add(R.drawable.p15);
        resList.add(R.drawable.p16);
        resList.add(R.drawable.p17);
        resList.add(R.drawable.p18);
        resList.add(R.drawable.p19);
    }

    public void linkViewsAndSetListener(View view) {
        parentLayout = (LinearLayout) view.findViewById(R.id.layout_play);
        subLayout = (RelativeLayout) view.findViewById(R.id.layout_sub);
        btnBack = (Button) view.findViewById(R.id.button_back);
        btnBack.setOnClickListener(this);
        btnStart = (Button) view.findViewById(R.id.button_start);
        btnStart.setOnClickListener(this);
        textTimer = (TextView) view.findViewById(R.id.text_timer);
    }

    public void showToast(String msg) {
        Toast.makeText(getActivity().getBaseContext(), msg, Toast.LENGTH_LONG).show();
    }
}
