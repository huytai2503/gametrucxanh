package edu.khtn.module03_baitap.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import edu.khtn.module03_baitap.R;

public class HomeFragment extends Fragment implements View.OnClickListener {
    Button btnPlay, btnLevel, btnSetting, btnScore;
    public static int chosenLevel = 4;
    public static int chosenTime = 60;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, null, false);
        linkView(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_play:
                addFragment(new PlayerFragment(), "player");
                break;
            case R.id.button_level:
                addFragment(new LevelFragment(), "level");
                break;
            case R.id.button_score:
                addFragment(new ScoreFragment(), "score");
                break;
            case R.id.button_setting:
                addFragment(new SettingFragment(), "setting");
                break;
        }
    }

    public void addFragment(Fragment fragment, String name) {
        Bundle bundle = new Bundle();
        bundle.putInt("chosenLevel", chosenLevel);
        bundle.putInt("chosenTime", chosenTime);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_main, fragment);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    public void linkView(View view) {
        btnPlay = (Button) view.findViewById(R.id.button_play);
        btnPlay.setOnClickListener(this);
        btnLevel = (Button) view.findViewById(R.id.button_level);
        btnLevel.setOnClickListener(this);
        btnScore = (Button) view.findViewById(R.id.button_score);
        btnScore.setOnClickListener(this);
        btnSetting = (Button) view.findViewById(R.id.button_setting);
        btnSetting.setOnClickListener(this);
    }

    public void showToast(String msg) {
        Toast.makeText(getActivity().getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
